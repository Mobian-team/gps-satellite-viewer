#!/usr/bin/env python3
import datetime
import gi
import os
import re
import sys
import tokenize

import importlib.resources as resources

import satellite.nmea as nmea
from .nmeasource import (
    ModemManagerNmeaSource,
    TestNmeaSource,
    ModemNoNMEAError,
    ModemLockedError,
    ModemError,
    NmeaSourceNotFoundError,
)
from .widgets import text_barchart, DataFrame

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Handy', '1')
from gi.repository import Gio, GLib, Gtk, Handy  # noqa: E402

VERSION="0.1.0"

class SatelliteApp(Gtk.Application):

    def __init__(self, *args, **kwargs):
        Gtk.Application.__init__(
            self, *args, application_id="org.example.satellite",
            flags=Gio.ApplicationFlags.FLAGS_NONE, **kwargs)
        Handy.init()

        progfile = os.path.realpath(sys.argv[0])
        basedir = os.path.join(os.path.dirname(progfile), '..')
        pkgdatadir = os.path.join(basedir, "data")

        self.builder = Gtk.Builder()
        self.builder.add_from_string(resources.read_text("satellite", "satellite.ui"))
        self.builder.add_from_string(resources.read_text("satellite", "menus.ui"))
        self.builder.connect_signals(self)
        for widget in self.builder.get_objects():
            if not isinstance(widget, Gtk.Buildable):
                continue
            # The following call looks ugly, but see Gnome bug 591085
            widget_name = Gtk.Buildable.get_name(widget)

            widget_api_name = '_'.join(re.findall(tokenize.Name, widget_name))
            if hasattr(self, widget_api_name):
                raise AttributeError(
                    "instance %s already has an attribute %s" % (
                        self, widget_api_name))
            else:
                setattr(self, widget_api_name, widget)
        self.connect("activate", self.do_activate)

        self.app_menu = self.builder.get_object('app-menu')
        self.menu_popover = Gtk.Popover.new_from_model(
            self.app_menu_button,
            self.app_menu)
        self.menu_popover.set_position(Gtk.PositionType.BOTTOM)

        self.infolabel.set_markup("<tt>" + "\n"*10 + "</tt>")

        self.lastfix_dt = datetime.datetime.today()
        self.dataframe = DataFrame()
#        self.dataframe.header.set_text("Satellite info")
        self.dataframe.header.set_visible(False)
        self.set_values({})  # Initialize dataframe
        self.dataframe.show()
        self.main_box.add(self.dataframe)

        self.set_speedlabel(None)
        self.leaflet.set_visible_child(self.databox)
        self.log_msg("Satellite started")

        try:
            self.source = ModemManagerNmeaSource(refresh_rate=5)
#            self.source = TestNmeaSource()
        except Exception:
            print("No NMEA source found")
            sys.exit(1)

        # Internal state
        self.last_mode = 1
        self.last_speed = None
        self.lastfix_dt = None

        GLib.timeout_add(1000, self.update, None)

    def create_actions(self):
        app_actions = (
            ('menu', self.on_menu),
            ('about', self.on_about),
            ('quit', self.on_quit),
        )
        for aname, cb in app_actions:
            action = Gio.SimpleAction.new(aname, None)
            action.connect('activate', cb)
            self.add_action(action)

    def do_startup(self):
        Gtk.Application.do_startup(self)
        self.create_actions()

    def do_activate(self, *args):
        self.add_window(self.window)
        self.window.show()
        return True

    def on_menu(self, action, param):
        self.menu_popover.popup()

    def on_about(self, *args):
        adlg = Gtk.AboutDialog(
            transient_for=self.window,
            modal=True,
            program_name="Satellite",
            version=VERSION,
            authors=("Teemu Ikonen <tpikonen@mailbox.org>",),
            comments="A program for showing navigation satellite data",
            license_type=Gtk.License.GPL_3_0_ONLY,
            copyright="Copyright 2021 Teemu Ikonen",
        )
        adlg.present()

    def on_quit(self, *args):
        self.quit()

    def leaflet_forward_cb(self, button):
        self.leaflet.navigate(Handy.NavigationDirection.FORWARD)
        return True

    def leaflet_back_cb(self, button):
        self.leaflet.navigate(Handy.NavigationDirection.BACK)
        return True

    def format_satellite_data(self, d):
        bchart = text_barchart(
            ((e['prn'], e['snr']) for e in d['visibles']),
            d['actives'], height=10)
        return bchart

    def set_values(self, data):
        def to_str(x, fmt="%s"):
            return fmt % x if x else "-"

        mode2fix = {
            "2": "2 D",
            "3": "3 D",
        }
        utcfmt = "%H:%M:%S UTC"
        datenow = datetime.datetime.utcnow()
        fixdate = data.get('date')
        fixdate = fixdate if fixdate else datenow.date()
        fixtime = data.get('fixtime')
        fixdt = datenow.combine(fixdate, fixtime) if fixtime else None
        self.lastfix_dt = fixdt if fixdt else self.lastfix_dt
        fixage = to_str((datenow - self.lastfix_dt).total_seconds(),
                        "%0.0f s") if self.lastfix_dt else "-"
        # Mapping: Data key, description, converter func
        order = [
            ("valid",   "Valid",        lambda x: "Yes" if x else "No"),
            ("actives", "Active sats",  lambda x: str(len(x))),
            ("visibles", "Visible sats", lambda x: str(len(x))),
            ("mode",    "Fix type",     lambda x: mode2fix.get(x, "No Fix")),
            ("latlon",  "Latitude",
                lambda x: "%0.6f" % x[0] if x else "-"),
            ("latlon",  "Longitude",
                lambda x: "%0.6f" % x[1] if x else "-"),
            ("fixtime", "Time of fix",
                lambda x: x.strftime(utcfmt) if x else "-"),
            ("valid",   "Sys. Time", lambda x: datenow.strftime(utcfmt)),
            ("fixtime", "Age of fix", lambda x: fixage),
            ("date",    "Date of fix",
                lambda x: x.strftime("%Y-%m-%d") if x else "-"),
            ("speed",   "Speed",        lambda x: to_str(x, "%0.1f m/s")),
            ("true_course", "True Course", lambda x: to_str(x, "%0.1f deg")),
            ("pdop", "PDOP", lambda x: to_str(x)),
            ("hdop", "HDOP", lambda x: to_str(x)),
            ("vdop", "VDOP", lambda x: to_str(x)),
        ]
        descs = []
        vals = []
        for key, desc, fun in order:
            if key not in data.keys():
                value = "N/A"
            else:
                value = fun(data[key])
            descs.append(desc)
            vals.append(value)
            print(f"{desc}: {value}")

        if self.dataframe.rows != len(descs):
            self.dataframe.set_rowtitles(descs)
        self.dataframe.set_values(vals)

    def set_status(self, mode):
        mode = int(mode) if mode else 0
        if mode == self.last_mode:
            return
        if mode == 2:
            image = 'face-smile-symbolic'
        elif mode == 3:
            image = 'face-cool-symbolic'
        else:
            image = 'face-crying-symbolic'
        self.left_status.set_from_icon_name(image, Gtk.IconSize.DND)
        self.right_status.set_from_icon_name(image, Gtk.IconSize.DND)
        self.last_mode = mode

    def set_speedlabel(self, speed):
        spd = str(int(3.6*speed)) if speed else "-"
        speedfmt = '<span size="50000">%s</span>\n<span size="30000">%s</span>'
        speedstr = speedfmt % (spd, "km/h")
        self.speedlabel.set_markup(speedstr)

    def log_msg(self, text):
        maxlines = 100  # Maximum num of lines in GtkLabel
        msg = datetime.datetime.now().strftime("[%H:%M:%S] ") + text
        text = self.loglabel.get_text().split('\n')
        if len(text) > maxlines:
            text = text[1:]
        text.append(msg)
        self.loglabel.set_text("\n".join(text))

    def update(self, x):
        try:
            nmeas = self.source.get()
        except Exception as e:
            fatal = False
            if isinstance(e, ModemLockedError):
                dtext = "Please unlock the Modem and restart"
                fatal = True
            elif isinstance(e, ModemNoNMEAError):
                dtext = "NMEA info not received with location"
            elif isinstance(e, ModemError):
                dtext = "Unknown modem error"
            elif isinstance(e, NmeaSourceNotFoundError):
                dtext = e.message if hasattr(e, 'message') else "Modem not found"
                fatal = True
            else:
                dtext = e.message if hasattr(e, 'message') else "Unknown error"
            if fatal:
                dialog = Gtk.MessageDialog(
                    parent=self.window, modal=True,
                    message_type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.OK,
                    text=dtext)
                dialog.set_title("Unrecoverable error")
                dialog.run()
                dialog.destroy()
                sys.exit(1)
            else:
                print(dtext)
                self.log_msg(dtext)
            return True

        data = nmea.parse(nmeas)
        barchart = self.format_satellite_data(data)
        print(barchart)
        self.infolabel.set_markup("<tt>" + barchart + "</tt>")
        self.set_values(data)
        mode = data["mode"]
        mode = int(mode) if mode else 0
        speed = data['speed']
        self.set_speedlabel(speed)
        if speed and not self.last_speed:
            self.carousel.scroll_to(self.speedlabel)
        elif not speed and self.last_speed:
            self.carousel.scroll_to(self.infolabel)
        self.last_speed = speed
        # log
        if mode != self.last_mode:
            if mode > 1:
                self.log_msg(f"Got lock, mode: {mode}")
            elif mode <= 1:
                self.log_msg("Lock lost")
        self.last_mode = mode
        return True
