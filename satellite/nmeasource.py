import satellite.modem_manager_defs as mm
from pydbus import SystemBus


class NmeaSourceNotFoundError(Exception):
    pass


class ModemError(Exception):
    pass


class ModemLockedError(ModemError):
    pass


class ModemNoNMEAError(ModemError):
    pass


class NmeaSource:
    def __init__(self):
        self.initialized = False

    def initialize(self):
        pass

    def get(self):
        pass


class ModemManagerNmeaSource(NmeaSource):
    def __init__(self, refresh_rate=1):
        super().__init__()
        self.bus = SystemBus()
        self.manager = self.bus.get('.ModemManager1')
        self.modem = None
        self.refresh_rate = refresh_rate

    def initialize(self):
        objs = self.manager.GetManagedObjects()
        mkeys = list(objs.keys())
        if mkeys:
            mstr = mkeys[0]
            print(f"Modem is: {mstr}")
        else:
            raise NmeaSourceNotFoundError("No Modems Found")
        self.modem = self.bus.get('.ModemManager1', mstr)

        try:
            self.modem.Setup(mm.MM_MODEM_LOCATION_SOURCE_GPS_NMEA, False)
        except AttributeError as e:
            print("Error during initialization")
            if self.modem.State == mm.MM_MODEM_STATE_LOCKED:
                raise ModemLockedError from e
            else:
                raise ModemError from e

        self.modem.SetGpsRefreshRate(self.refresh_rate)
        self.initialized = True

    def get(self):
        if not self.initialized:
            self.initialize()
        retry = 0
        try:
            loc = self.modem.GetLocation()
        except Exception as e:
            print(f"exception in nmeasource.get(): {e.message}")
            self.initialized = False
            raise e

        retval = loc.get(mm.MM_MODEM_LOCATION_SOURCE_GPS_NMEA)
        if retval is None:
            print(f"Location does not have NMEA information")
            self.initialized = False
            raise ModemNoNMEAError
        return retval


class TestNmeaSource(NmeaSource):
    nmeas = [
        "$GPGSV,1,1,02,25,,,29,32,,,30,1*68\r\n"
        "$GPGSA,A,1,,,,,,,,,,,,,,,,*32\r\n"
        "$GPRMC,,V,,,,,,,,,,N*53\r\n"
        "$GPVTG,,T,,M,,N,,K,N*2C\r\n"
        "$GPGGA,,,,,,0,,,,,,,,*66\r\n",
    ]

    def initialize(self):
        self.initialized = True

    def get(self):
        return self.nmeas[0]
