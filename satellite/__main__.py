import sys
from .application import SatelliteApp


def main():
    app = SatelliteApp()
    app.run()
    sys.exit(0)


if __name__ == '__main__':
    main()
