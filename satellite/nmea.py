import pynmea2
from collections import OrderedDict

MS_PER_KNOT = 0.514444


def dget(key):
    def fn(d):
        return d.get(key)
    return fn


def fget(key, scale=1.0):
    def fn(d):
        try:
            return scale * float(d.get(key))
        except ValueError:
            return None
    return fn


def get_time(d):
    itime = d.get("timestamp")
    if itime:
        return pynmea2.nmea_utils.timestamp(itime)
    else:
        return None


def get_date(d):
    adate = d.get("datestamp")
    if adate:
        return pynmea2.nmea_utils.datestamp(adate)
    else:
        return None


def get_latlon(mdict):
    lat = mdict.get('lat')
    lon = mdict.get('lon')
    lat_dir = mdict.get('lat_dir')
    lon_dir = mdict.get('lon_dir')
    if not all((lat, lon, lat_dir, lon_dir)):
        return None
    lat_deg = float(lat[:2])
    lat_min = float(lat[2:])
    lon_deg = float(lon[:3])
    lon_min = float(lon[3:])
    flat = lat_deg + lat_min/60
    flat = -1 * flat if lat_dir == 'S' else flat
    flon = lon_deg + lon_min/60
    flon = -1 * flon if lon_dir == 'W' else flon
    return (flat, flon)


def get_mag_variation_rmc(mdict):
    try:
        val = float(mdict.get('mag_variation'))
        return -1 * val if mdict.get('mag_var_dir') == 'W' else val
    except ValueError:
        return None


# Functions for getting and converting values from parsed NMEA message dicts
getters = {
    "latlon": OrderedDict((
        ('RMC', get_latlon),
        ('GGA', get_latlon),
    )),
    "fixtime": OrderedDict((
        ('RMC', get_time),
        ('GGA', get_time),
    )),
    "date": OrderedDict((
        ('RMC', get_date),
    )),
    "valid": OrderedDict((
        ('RMC', lambda x: x.get('status') == 'A'),
    )),
    "speed": OrderedDict((
        ('RMC', fget('spd_over_grnd', MS_PER_KNOT)),
        ('VTG', fget('spd_over_grnd_kts', MS_PER_KNOT)),
    )),
    "true_course": OrderedDict((
        ('RMC', fget('true_course')),
        ('VTG', fget('true_track')),
    )),
    "mag_course": OrderedDict((
        ('VTG', fget('mag_track')),
    )),
    "mag_variation": OrderedDict((
        ('RMC', get_mag_variation_rmc),
    )),
}


def msg_get(msgs, var):
    gdict = getters[var]
    try:
        return next(gdict[k](v) for k,v in msgs.items() if k in gdict.keys())
    except StopIteration:
        return None


def __get_fields(msg):
    out = {}
    for i, data in enumerate(msg.data):
        if i >= len(msg.fields):
            break
        out[msg.fields[i][1]] = data
    return out


def parse(nmeas):
    visibles = []
    actives = []
    sel_mode = None
    mode = None
    pdop = None
    hdop = None
    vdop = None
    msgs = {}

    parsed = [pynmea2.parse(n) for n in nmeas.split('\r\n') if n]
    for msg in parsed:
        #print(repr(msg))
        keys = []
        for field in msg.fields:
            keys.append(field[1])
        if isinstance(msg, pynmea2.types.GSV):
            for n in range(1, (len(msg.data) - 4) // 4 + 1):
                if f'sv_prn_num_{n}' in keys:
                    visibles.append({
                        'prn': getattr(msg, f'sv_prn_num_{n}'),
                        'elevation': getattr(msg, f'elevation_deg_{n}'),
                        'azimuth': getattr(msg, f'azimuth_{n}'),
                        'snr': getattr(msg, f'snr_{n}'),
                    })
                else:
                    break
        elif isinstance(msg, pynmea2.types.GSA):
            sel_mode = getattr(msg, 'mode')
            mode = getattr(msg, 'mode_fix_type')
            pdop = getattr(msg, 'pdop')
            hdop = getattr(msg, 'hdop')
            vdop = getattr(msg, 'vdop')
            for n in range(1, 12+1):
                val = getattr(msg, f'sv_id{n:02d}')
                if val != '':
                    actives.append(val)
        elif isinstance(msg, pynmea2.types.GGA):
            msgs['GGA'] = __get_fields(msg)
        elif isinstance(msg, pynmea2.types.RMC):
            msgs['RMC'] = __get_fields(msg)
        elif isinstance(msg, pynmea2.types.VTG):
            msgs['VTG'] = __get_fields(msg)

    out = {
        "visibles": visibles,
        "actives": actives,
        "sel_mode": sel_mode,
        "mode": mode,
        "pdop": pdop,
        "hdop": hdop,
        "vdop": vdop,
    }
    out.update({k: msg_get(msgs, k) for k in getters.keys()})
    return out
