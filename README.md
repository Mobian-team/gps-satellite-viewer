Satellite is a program which displays global navigation satellite system
(GNSS: GPS et al.) data obtained from modemmanager API.

## Dependencies:

    gi, Gtk, libhandy, pydbus, pynmea2

## Running and installing

### From source tree

Run the script bin/satellite.

### With pip

Run

    pip3 install --user ./

in the source tree root.

### Flatpak

Run

    flatpak-builder --install --user build-dir flatpak/org.codeberg.tpikonen.satellite.json

in the source tree root. Then run

    flatpak run org.codeberg.tpikonen.satellite

to execute.
